import QtQuick 2.12
import io.thp.pyotherside 1.5
import Lomiri.Components 1.3


/*!
    \brief MainView with a Label and Button elements.
*/

MainView {
    id: root
    objectName: 'mainView'

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: 'wifitransfer.aloysliska'

    /*
     This property enables the application to change orientation
     when the device is rotated. The default is false.
    */
    automaticOrientation: false

    width: units.gu(40)
    height: units.gu(71)
    backgroundColor: "#6A69A2"
    footerColor: "#8896D5"

    //----------------------
    // Application settings
    //----------------------
    // url of file to export via Content Hub
    property string fileUrltoExport: ""

    PageStack {
        id: pageStack

        Component.onCompleted: {
            pageStack.push(Qt.resolvedUrl("wifiTransferPage.qml"))
        }
    }

    // Reception of python events and data are centralized below in py
    // signals are used to redirect events to qml object (with Connections to the signal)
    signal pyfsStarted(var lblParam)
    signal pyfsStopped()
    signal pyfsConnected(bool lblconnectedStatus)
    signal pyfsFolder(var pydataFolder)
    signal pyfsFileReceived(var pyFileNames)

    Python {
        id: py
        Component.onCompleted: {
            // Add the directory of this .qml file to the search path
            addImportPath(Qt.resolvedUrl('../src'));
            importModule('fserver', function () {
                console.log("Python module loaded");
            });
        }
        onError: console.log('Python error: ' + traceback)
        onReceived: {
            console.log("got python data", data);
            if (Array.isArray(data) && data[0] == "started") {
                root.pyfsStarted(data)
            }
            else if (data == "stopped") {
                root.pyfsStopped()
            }
            else if (data == "files_use_file_manager") {
                console.log("unlocked, fail")               // TODO understand what that means!
            }
            else if (Array.isArray(data) && data[0] == "files") {
                root.pyfsFileReceived(data)
            }
            else if (Array.isArray(data) && data[0] == "writeabledir") {
                root.pyfsFolder(data)
            }
            else if (Array.isArray(data) && data[0] == "user_connected") {
                root.pyfsConnected(true)
            }
            else if (Array.isArray(data) && data[0] == "user_disconnected") {
                root.pyfsConnected(false)
            }
        }
    }
}

