import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Content 1.1


// Page to export a file from  wifiTransfer app to another app.
//   file to be exported is stored in wifiTransfer app folder which is used for ftp

Page {
    id: exportPage
    visible: false

    //tranfer that will be set by ContentPeerPicker
    property var cTransfer

    header: PageHeader {
        id: exportHeader
        title: i18n.tr('Export File to App:')
        StyleHints {
            backgroundColor: "#343C60"
            dividerColor: LomiriColors.slate
        }
    }

    ContentItem {
        id: exportItem
        text: i18n.tr("Transferred file")
        url:fileUrltoExport
    }

    ContentPeerPicker {
        anchors.fill: parent
        visible: parent.visible
        showTitle: false

        contentType: ContentType.All            // any type of files
        handler: ContentHandler.Destination     // to export file to another app

        onPeerSelected: {
            peer.selectionType = ContentTransfer.Single
            var items = new Array();
            items.push(exportItem);
            exportPage.cTransfer = peer.request()
            exportPage.cTransfer.stateChanged.connect(function() {
                if (exportPage.cTransfer.state === ContentTransfer.InProgress) {
                    console.log("Export: In progress");
                    exportPage.cTransfer.items = items
                    exportPage.cTransfer.state = ContentTransfer.Charged;
                    pageStack.pop()
                }
            })
        }

        onCancelPressed: {
            pageStack.pop()
        }
    }

    // Display a waiting screen during transfer
    ContentTransferHint {
        id: transferHint
        anchors.fill: parent
        activeTransfer: exportPage.cTransfer
    }
}
