import QtQuick 2.12
import Lomiri.Components 1.3

Page {
    id: helpPage
    visible: false

    header: PageHeader {
        id: helpHeader
        title: i18n.tr('Help')
        StyleHints {
            backgroundColor: "#343C60"
            dividerColor: LomiriColors.slate
        }
    }

    Flickable {
        anchors.top: helpHeader.bottom
        width: parent.width
        height: parent.height - helpHeader.height
        contentHeight: column1.height

        Column {
            id: column1
            spacing: units.gu(2)
            topPadding: units.gu(2)
            anchors {
                margins: units.gu(3)
                horizontalCenter: parent.horizontalCenter
            }
            width: parent.width - units.gu(4)

            Label {
                id: label1
                width: parent.width
                textSize: Label.Medium
                wrapMode: Text.Wrap
                text: i18n.tr("<b>WifiTransfer</b> uses File Transfer Protocol (FTP) to transfer files through Wi-Fi. FTP is a way to transfer files between devices over the network.<br> On your phone, WifiTransfer runs a FTP server.<br> On your remote computer, you need to run a FTP client and connect to the FTP server.<br> Connection requires a username and a password which are provided by WifiTransfer. Once connected, you can transfer files between your phone and your remote computer.")
            }

            Label {
                id: label2
                width: parent.width
                textSize: Label.Medium
                wrapMode: Text.Wrap
                text: i18n.tr("<b>To receive files from your remote computer:</b> WifiTransfer has a <i>shared folder</i> on your phone which is visible from your FTP client on your remote computer. Add files to this <i>shared folder</i> to transfer them to your phone. Then, on your phone, click on the file to share it with another app (Gallery, File Manager,...).")
            }

            Label {
                id: label3
                width: parent.width
                textSize: Label.Medium
                wrapMode: Text.Wrap
                text: i18n.tr("<b>To send files to your remote computer:</b> select the files with WifiTransfer. WifiTransfer will make a copy of selected files in the subfolder <i>Contents</i> of the <i>share folder</i> and it will be automatically transferred to your remote computer.")
            }

            Label {
                id: label4
                width: parent.width
                textSize: Label.Large
                wrapMode: Text.Wrap
                text: i18n.tr("<b>Security warning:</b>")
            }

            Label {
                id: label5
                width: parent.width
                textSize: Label.Medium
                wrapMode: Text.Wrap
                text: i18n.tr("<b>FTP is not secure</b>. FTP transmits data, including usernames and passwords, in plain text. This means that anyone intercepting the data can easily read it.")
            }
        }
    }
}
